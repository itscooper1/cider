#!/usr/bin/python3
import netaddr, sys, argparse, subprocess, time, ipaddress
from os import path

parser = argparse.ArgumentParser(description='This tool takes a file with IP addresses or CIDR ranges as input and will try to compress the values into broader CIDRs to save space.')
parser.add_argument("input_file",type=str,help="File containing IP addresses")
args = parser.parse_args()

IP_dict={}
ranges=[]

if path.exists(args.input_file):
	with open(args.input_file, "r") as IPs:
		for IP in IPs:
			IP=IP.rstrip()
			if "\\"in IP:
				base,CIDR=IP.
			IP_dict[int(ipaddress.IPv4Address(IP))]=IP
	sorted_IPs=sorted(IP_dict.keys())
	set_base = "None"
	previous = "None"
	for key in sorted_IPs:
		IP = str(IP_dict[key])
		if set_base=="None" and previous=="None":
			set_base=IP
			previous=set_base
			continue
		else:
			binary_previous=int(ipaddress.IPv4Address(previous))
			binary_IP=int(ipaddress.IPv4Address(IP))
			if binary_previous+1==binary_IP:
				previous=IP
				continue
			else:
				ranges.append(str(set_base)+"-"+str(previous))
				set_base=IP
				previous=IP
else:
	print("Could not find file, try again.")
	sys.exit()

results=[]
for IPrange in ranges:
	start,end = IPrange.split("-")
	cidrs = netaddr.iprange_to_cidrs(start, end)
	for ipnetwork in cidrs:
		results.append(ipnetwork)
		print(ipnetwork)